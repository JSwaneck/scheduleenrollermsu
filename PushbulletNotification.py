import requests
import json

USER_API_KEY = ""


def send_pushbullet_notification(title, body):
    if USER_API_KEY == "":
        return 1

    url = "https://api.pushbullet.com/v2/pushes"
    data = {"type": "note", "title": title, "body": body}
    headers = {
        "Authorization": "Bearer " + USER_API_KEY,
        'Content-Type': 'application/json'
    }
    response = requests.post(url, data=json.dumps(data), headers=headers)
    if response.status_code != 200:
        return 1
    return 0

def test_notification():
    failure = send_pushbullet_notification("Test Notification", "Hello World!")
    if failure:
        print("Failed to send test notification.")
    else:
        print("Successfully sent test notification!")