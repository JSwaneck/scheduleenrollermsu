# ScheduleEnrollerMSU

This python script for Michigan State students is designed to auto-enroll students in their planned courses on schedule builder whenever a spot opens up.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

1. A computer you are willing to let run all day
2. [ChromeDriver](http://chromedriver.chromium.org/) - For automating the schedule builder
3. [Pushbullet Account](https://www.pushbullet.com/) - (Optional) For notifications on your mobile device

### Installing

1. Download this repo onto your computer
2. Open ScheduleEnroller.py and enter:
	1. Location of chromedriver.exe you downloaded
	2. Your MSU Login information
    3. The semester you wish to check (E.g. "Spring 2020")
3. Run ScheduleEnroller.py

### Optional Pushbullet Notiications

1. Create a pushbullet account
2. Generate an Access Token on your [Account Settings](https://www.pushbullet.com/#settings/account) page.
3. Insert the key into PushbulletNotification.py
4. Run the test_notification() function to verify it works
5. Enjoy!

## Credits

* **David Evenson** - *Original Creator* - April 2017

* **Jacob Swaneck** - *Current Maintainer* - December 2018



Suggestions? Let me know!

swaneckj@msu.edu
