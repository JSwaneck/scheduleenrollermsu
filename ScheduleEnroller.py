# MSU Schedule Auto Enroller

# Originally created by
# David Evenson - April 2017

# Updated by
# Jacob Swaneck - December 2018

import time
import sys

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, UnexpectedAlertPresentException, TimeoutException, StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait, Select

from PushbulletNotification import send_pushbullet_notification


# What this program does #

# This program logs you into MSU's schedule builder
# and continuously checks your Planned Courses for openings.
# When one is found it attempts enroll you automatically.
# NOTE: It currently cannot swap classes or handle course conflicts.
#
# It has the ability to notify you using Pushbullet
#   It sends a notification when an opening is found and another once enrolled
# For this you will need an account and an API key which you can generate on their site

# Instructions #

# Download Chrome Driver (http://chromedriver.chromium.org/)
#   Save chrome driver somewhere on your computer
#   Link chromedriver.exe in the line below
# Add your Username and Password below that
# Choose the target semester, blank for current semester
# To enable PushBullet notifications, add your account's API key to PushbulletNotifications.py

# ChromeDriver #
driver = webdriver.Chrome("C:\Program Files (x86)\ChromeDriver\chromedriver.exe")

# Login Info #
username = ""
password = ""

# Target Semester #
# ex: "Spring 2020", "Fall 2019", "Summer 2021"
semester = ""
# Delay between page refreshes (in seconds) #
delay = 30


def login():
    logged_in = False
    timeout = 4
    while not logged_in:
        try:
            driver.delete_all_cookies()
            driver.get("https://schedule.msu.edu/Planner.aspx")
            w = WebDriverWait(driver, timeout).until(
                EC.presence_of_element_located((By.ID, "netid")))
            NetID = driver.find_element_by_id("netid")
            Password = driver.find_element_by_id("pswd")
            #
            # # send keys
            NetID.send_keys(username)
            Password.send_keys(password)
            Password.send_keys(Keys.ENTER)
            logged_in = True
            # Schedule Builder page
        except TimeoutException:
            print("Login failed")
            # Attempt to log out and log back in to get around MSU's timed-logout system
            try:
                logout()
                print("Logged out - Attempting login again")
            except TimeoutException:
                print("Unable to logout - Definitely check internet")
            timeout *= 2


def logout():
    w = WebDriverWait(driver, 3).until(
        EC.presence_of_element_located((By.ID, "MainContent_Logout")))
    logout_btn = driver.find_element_by_id("MainContent_Logout")
    logout_btn.click()



def check_all_alerts_on_current_page():

    current_planned_course = 0
    print("Checking all courses")

    # Wait until page is loaded
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "course-results-table")))

    while 1:
        # Check to see if we are on the correct semester
        semester_dropdown = driver.find_element_by_id("MainContent_ddlTerm")
        semester_select = Select(semester_dropdown)
        selected_semester = semester_select.first_selected_option
        if semester != "" and semester not in selected_semester.text:
            # Must select correct semester
            dropdown_options = semester_dropdown.find_elements_by_tag_name("option")
            index = -1
            for i in range(0, len(dropdown_options)):
                if semester in dropdown_options[i].text:
                    index = i
            if index != -1:
                # Select new semester and wait until page load
                semester_select.select_by_index(index)
                complete = False
                delay = 1
                # TODO: Replace this with selenium wait for text_to_be_present_in_element
                while not complete:
                    WebDriverWait(driver, 1)
                    try:
                        header = driver.find_element_by_id("MainContent_UCEnrl_tdHeader").text
                        if semester not in header:
                            delay *= 2
                        else:
                            complete = True
                    except StaleElementReferenceException:
                        # This exception happens if finding element during page refresh
                        delay *= 2
            else:
                print("ERROR: Please provide a valid semester, '" + semester + "' is not found")

        # html ID's for current row
        row_id = f"MainContent_UCPlan_rptPlanner_trMeeting_{current_planned_course}"
        button_wrapper_id = f"MainContent_UCPlan_rptPlanner_tdAction_{current_planned_course}"
        enroll_element_id = f"MainContent_UCPlan_rptPlanner_Image2_{current_planned_course}"
        alert_element_id = f"MainContent_UCPlan_rptPlanner_Image4_{current_planned_course}"
        status_element_id = f"MainContent_UCPlan_rptPlanner_tdStatus_{current_planned_course}"
        course_name_id = f"MainContent_UCPlan_rptPlanner_tdCourse_{current_planned_course}"
        course_section_id = f"MainContent_UCPlan_rptPlanner_tdSection_{current_planned_course}"

        try:
            row = driver.find_element_by_id(row_id)
            enroll = row.find_element_by_id(enroll_element_id)
            able_to_enroll = True

            # Search for alert button
            wrapper = row.find_element_by_id(button_wrapper_id)
            action_buttons = wrapper.find_elements_by_tag_name("img")
            for i in range(0, len(action_buttons)):
                if action_buttons[i].id == alert_element_id:
                    # Alert button found - Class still full
                    able_to_enroll = False

            # check status text
            status = row.find_element_by_id(status_element_id).text.lower()
            if "restricted" in status or "section full" in status:
                able_to_enroll = False

            # Get course name for printing
            course = row.find_element_by_id(course_name_id).find_elements_by_tag_name("a")[0].text
            section = row.find_element_by_id(course_section_id).find_elements_by_tag_name("a")[0].text

            # Attempt to enroll
            if able_to_enroll:

                print(f"{course} {section} OPEN")
                print(" - Attempting to enroll")
                if send_pushbullet_notification(f"{course} {section} OPEN",
                                                f"{course} {section} has an open seat. Attempting to enroll"):
                    print("Unable to send pushbullet notification.")

                enroll.click()
                WebDriverWait(driver, 30).until(
                    EC.presence_of_element_located((
                        By.ID, "MainContent_btnContinue")))
                driver.find_element_by_id("MainContent_btnContinue").click()
                time.sleep(5)

                print(" - Enrollment successful, returning to planner")
                if send_pushbullet_notification("Enrollment successful!",
                                                f"Successfully enrolled in course {course} section {section}"):
                    print("Unable to send pushbullet notification.")

                driver.get("https://schedule.msu.edu/Planner.aspx")
                return
            current_planned_course += 1
        except NoSuchElementException:
            print("All courses checked")
            print("-------------------")
            return


# TODO: Update this
def checkAlertsF(enroll_url, alert_url):
    # go to fall
    driver.find_element_by_xpath(
        "//*[@id='MainContent_ddlTerm']/option[2]").click()

    w = WebDriverWait(driver, 30).until(EC.text_to_be_present_in_element(
        (By.XPATH, "//*[@id='MainContent_UCEnrolled_tdHeader']/h3[1]"),
        "Enrolled Courses for Fall 2017"))

    # check for enroll button
    try:
        enroll = driver.find_element_by_id(enroll_url)
        try:
            # check for alert button
            alert = driver.find_element_by_id(alert_url)
            print("section full")
        except NoSuchElementException:
            enroll.click()
            w = WebDriverWait(driver, 30).until(
                EC.presence_of_element_located((
                    By.ID, "MainContent_btnContinue")))
            driver.find_element_by_id("MainContent_btnContinue").click()
            print("Click enroll button")
            time.sleep(5)
            driver.get("https://schedule.msu.edu/Planner.aspx")
            w = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.ID, "MainContent_updpnl")))
    except NoSuchElementException:
        print("no enroll button / already enrolled")


# TODO: Update this
def checkAlertsS(enroll_url, alert_url):
    # go to spring
    driver.find_element_by_xpath(
        "//*[@id='MainContent_ddlTerm']/option[2]").click()

    w = WebDriverWait(driver, 30).until(EC.text_to_be_present_in_element(
        (By.XPATH, "//*[@id='MainContent_UCEnrolled_tdHeader']/h3[1]"),
        "Enrolled Courses for Spring 2018"))

    # check for enroll button
    try:
        enroll = driver.find_element_by_id(enroll_url)
        try:
            # check for alert button
            alert = driver.find_element_by_id(alert_url)
            # print("section full")
        except NoSuchElementException:
            driver.execute_script("arguments[0].click();", enroll)
            w = WebDriverWait(driver, 30).until(
                EC.presence_of_element_located((
                    By.ID, "MainContent_btnContinue")))
            enroll2 = driver.find_element_by_id("MainContent_btnContinue")
            driver.execute_script("arguments[0].click();", enroll2)
            print("Click enroll button")
            time.sleep(5)
            driver.get("https://schedule.msu.edu/Planner.aspx")
            w = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.ID, "MainContent_updpnl")))
            sys.exit()
    except NoSuchElementException:
        print("no enroll button / already enrolled")


while 1:
    # Program Start #
    print("Starting Schedule Enroller!")

    # Time & Day #
    hour = time.strftime('%H')
    print(hour)
    day = time.strftime('%w')
    print(day)

    # TODO: Verify these times
    weekend = False
    if int(day) == 6 and int(hour) > 8:
        weekend = True
    if int(day) < 6 and int(hour) > 8:
        weekend = False

    runCount = 0
    print("attempting login")
    login()

    while 7 <= int(hour) < 20 or weekend:
        hour = time.strftime('%H')

        runCount = runCount + 1
        print(runCount)

        try:
            check_all_alerts_on_current_page()
            time.sleep(delay)
            driver.refresh()
            if driver.current_url != "https://schedule.msu.edu/Planner.aspx":
                login()
        except TimeoutException:
            print("Operation timed out - Attempting login")
            login()
        except UnexpectedAlertPresentException:
            # Yes this is depricated, no I haven't found another way that works
            driver.switch_to_alert().dismiss()
            print("MSU Logout Alert on screen - Attempting login")
            login()

    if driver.current_url != "https://www.google.com":
        driver.get("https://www.google.com")
    time.sleep(60)
